export const data = [
  {
    release: 'Mar 2019',
    manufacturer: 'Bitmain',
    model: 'S9i',
    hash: '10.5-14.5 th/s',
    algorithm: 'SHA-256',
    efficiency: '8.00j/H/s',
    profitPerDay: 122.8,
    price: '$137.00 - $217.00'
  },
  {
    release: 'Mar 2019',
    manufacturer: 'Bitmain',
    model: 'S9i',
    hash: '10.5-14.5 th/s',
    algorithm: 'SHA-256',
    efficiency: '8.00j/H/s',
    profitPerDay: 0.08,
    price: '$137.00 - $217.00'
  },
  {
    release: 'Mar 2019',
    manufacturer: 'Bitmain',
    model: 'S9i',
    hash: '10.5-14.5 th/s',
    algorithm: 'SHA-256',
    efficiency: '8.00j/H/s',
    profitPerDay: 0.8,
    price: '$137.00 - $217.00'
  },
  {
    release: 'Mar 2019',
    manufacturer: 'Bitmain',
    model: 'S9i',
    hash: '10.5-14.5 th/s',
    algorithm: 'SHA-256',
    efficiency: '8.00j/H/s',
    profitPerDay: 0.58,
    price: '$137.00 - $217.00'
  },
  {
    release: 'Mar 2019',
    manufacturer: 'Bitmain',
    model: 'S9i',
    hash: '10.5-14.5 th/s',
    algorithm: 'SHA-256',
    efficiency: '8.00j/H/s',
    profitPerDay: 10.8,
    price: '$137.00 - $217.00'
  },
  {
    release: 'Mar 2019',
    manufacturer: 'Bitmain',
    model: 'S9i',
    hash: '10.5-14.5 th/s',
    algorithm: 'SHA-256',
    efficiency: '8.00j/H/s',
    profitPerDay: 152.1,
    price: '$137.00 - $217.00'
  },
  {
    release: 'Mar 2019',
    manufacturer: 'Bitmain',
    model: 'S9i',
    hash: '10.5-14.5 th/s',
    algorithm: 'SHA-256',
    efficiency: '8.00j/H/s',
    profitPerDay: 100.2,
    price: '$137.00 - $217.00'
  },
  {
    release: 'Mar 2019',
    manufacturer: 'Bitmain',
    model: 'S9i',
    hash: '10.5-14.5 th/s',
    algorithm: 'SHA-256',
    efficiency: '8.00j/H/s',
    profitPerDay: 5.8,
    price: '$137.00 - $217.00'
  },
  {
    release: 'Mar 2019',
    manufacturer: 'Bitmain',
    model: 'S9i',
    hash: '10.5-14.5 th/s',
    algorithm: 'SHA-256',
    efficiency: '8.00j/H/s',
    profitPerDay: 4.3,
    price: '$137.00 - $217.00'
  },
  {
    release: 'Mar 2019',
    manufacturer: 'Bitmain',
    model: 'S9i',
    hash: '10.5-14.5 th/s',
    algorithm: 'SHA-256',
    efficiency: '8.00j/H/s',
    profitPerDay: 0.8,
    price: '$137.00 - $217.00'
  },
  {
    release: 'Mar 2019',
    manufacturer: 'Bitmain',
    model: 'S9i',
    hash: '10.5-14.5 th/s',
    algorithm: 'SHA-256',
    efficiency: '8.00j/H/s',
    profitPerDay: 0.55,
    price: '$137.00 - $217.00'
  },
  {
    release: 'Mar 2019',
    manufacturer: 'Bitmain',
    model: 'S9i',
    hash: '10.5-14.5 th/s',
    algorithm: 'SHA-256',
    efficiency: '8.00j/H/s',
    profitPerDay: 33.2,
    price: '$137.00 - $217.00'
  },
  {
    release: 'Mar 2019',
    manufacturer: 'Bitmain',
    model: 'S9i',
    hash: '10.5-14.5 th/s',
    algorithm: 'SHA-256',
    efficiency: '8.00j/H/s',
    profitPerDay: 7.66,
    price: '$137.00 - $217.00'
  },
  {
    release: 'Mar 2019',
    manufacturer: 'Bitmain',
    model: 'S9i',
    hash: '10.5-14.5 th/s',
    algorithm: 'SHA-256',
    efficiency: '8.00j/H/s',
    profitPerDay: 2.5,
    price: '$137.00 - $217.00'
  },
  {
    release: 'Mar 2019',
    manufacturer: 'Bitmain',
    model: 'S9i',
    hash: '10.5-14.5 th/s',
    algorithm: 'SHA-256',
    efficiency: '8.00j/H/s',
    profitPerDay: 110.1,
    price: '$137.00 - $217.00'
  },
];