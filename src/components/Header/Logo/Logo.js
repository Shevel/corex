import React from 'react';
import image from '../../../assets/images/Logo.svg';
import styles from './Logo.module.scss';

export const Logo = () => {
  return (
    <img className={styles.logo} src={image} alt='Corex'/>
  )
}