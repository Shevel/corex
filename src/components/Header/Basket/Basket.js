import React from 'react';
import basket from '../../../assets/Icons/basket.svg'
import styles from './Basket.module.scss';

export const Basket = () => {
  return (
    <div className={styles.basket}>
      <img className={styles.basket_svg} src={basket} alt='basket-icon'/>
      <span className={styles.basket__value}>0</span>
    </div>
  )
}