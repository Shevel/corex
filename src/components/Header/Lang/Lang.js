import React, { useState } from 'react';
import earth from '../../../assets/Icons/earth.svg';
import styles from './Lang.module.scss';

export const Lang = () => {
  const [showOptions, setShowOptions] = useState(false);
  const toggleShowOptions = () => {
    setShowOptions(!showOptions);
  }
  return (
    <React.Fragment>
      <div className={styles.lang_block}>
        <div 
          className={styles.lang__top}
          onClick={toggleShowOptions}
        >
          <img className={styles.lang__earth_icon} src={earth} alt='earth-icon' />
          <span
            className={styles.lang__current_lang}
          >ENG
          </span>
        </div>
        {
          showOptions ?
            <div className={styles.lang__options}>
              <ul className={styles.lang__items_list}>
                <li className={styles.lang__item}>ESP</li>
                <li className={styles.lang__item}>RUS</li>
                <li className={styles.lang__item}>CHN</li>
                <li className={styles.lang__item}>KOR</li>
              </ul>
            </div>
            : null
        }

      </div>
    </React.Fragment>
  )
}