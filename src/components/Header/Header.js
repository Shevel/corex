import React from 'react';
import styles from './Header.module.scss';
import { Logo } from './Logo/Logo';
import { Lang } from './Lang/Lang';
import { Basket } from './Basket/Basket';

export const Header = () => {
  return (
    <header className={styles.header}>
      <div className={styles.header__logo_block}>
        <div className={styles.header__logo}>
          <Logo />
        </div>
        <Lang />
      </div>
      <Basket />
    </header>
  )
}