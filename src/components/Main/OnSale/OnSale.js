import React from 'react';
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import './Table.scss';
import styles from './OnSale.module.scss';
import { data } from '../../../data';
import star from '../../../assets/Icons/star.svg';
import greenArrow from '../../../assets/Icons/greenArrow.svg';

const useStyles = makeStyles({
  root: {
    width: '100%',
    display: 'table',
    borderSpacing: '0 15px',
    borderCollapse: 'inherit',
  },
  tableHead: {
    backgroundColor: '#ffffff0d',
    width: '100%',
  },
  headRow: {
    width: '100%',
  },
  bodyRow: {
    width: '100%',
  },
  headCell: {
    position: 'relative',
    fontFamily: 'Open Sans',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: '12px',
    lineHeight: '15px',
    color: '#FFFFFF',
    padding: '9px 0 9px 0 ',
    borderBottom: '1px solid #ffffff1a',
    borderTop: '1px solid #ffffff1a',
    emptyCells: 'show',
  },
  bodyCell: {
    position: 'relative',
    fontFamily: 'Open Sans',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: '12px',
    lineHeight: '15px',
    color: '#C4C4C4',
    borderBottom: 'none',
    padding: '12px 0 12px 0 ',
    emptyCells: 'show',
  },
  green: {
    color: '#00A651',
  },
  text_secondary: {
    color: '#C4C4C4',
  },
  text_primary: {
    color: '#FFF',
  },
  star: {
    marginLeft: '15px',
  },
  green_arrow: {
    marginLeft: '15px',
    marginRight: '15px',
  },
});

export const OnSale = () => {
  const classes = useStyles();
  const tableElements = data.map(
    (element, i) => {
      return (
        <TableRow key={data.length - i} className={classes.bodyRow}>
          <TableCell
            align='center'
            className={classes.bodyCell}>
            {
              <React.Fragment>
                <img className={classes.star} src={star} alt='star' />
                <img className={classes.green_arrow} src={greenArrow} alt='arrow' />
              </React.Fragment>
            }
          </TableCell>
          <TableCell align='center' className={classes.bodyCell}>{element.release}</TableCell>
          <TableCell align='center' className={classes.bodyCell}>{element.manufacturer}</TableCell>
          <TableCell align='center' className={classes.bodyCell}>{element.model}</TableCell>
          <TableCell align='center' className={classes.bodyCell}>{element.hash}</TableCell>
          <TableCell align='center' className={classes.bodyCell}>{element.algorithm}</TableCell>
          <TableCell align='center' className={classes.bodyCell}>{element.efficiency}</TableCell>
          <TableCell
            align='center'
            className={`${classes.bodyCell} ${classes.green}`}>
            {element.profitPerDay}
            <span className={classes.text_secondary}> / per day</span>
          </TableCell>
          <TableCell
            align='center'
            className={`${classes.bodyCell} ${classes.text_primary}`}>
            {element.price}
          </TableCell>
        </TableRow>)
    }
  )
  return (
    <div className={styles.onSale}>
      <h3 className={styles.onSale__title}>ON SALE</h3>
      <Table className={classes.root}>
        <TableHead className={classes.tableHead}>
          <TableRow className={classes.tableRow}>
            <TableCell align='center' className={classes.headCell}></TableCell>
            <TableCell align='center' className={classes.headCell}>Release</TableCell>
            <TableCell align='center' className={classes.headCell}>Manufacturer</TableCell>
            <TableCell align='center' className={classes.headCell}>Model</TableCell>
            <TableCell align='center' className={classes.headCell}>Hash</TableCell>
            <TableCell align='center' className={classes.headCell}>Algorithm</TableCell>
            <TableCell align='center' className={classes.headCell}>Efficiency</TableCell>
            <TableCell align='center' className={classes.headCell}>Profit</TableCell>
            <TableCell align='center' className={classes.headCell}>Price</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {tableElements}
        </TableBody>
      </Table>
    </div>
  )
}