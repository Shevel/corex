import React from 'react';
import styles from './News.module.scss';
import banner from '../../../assets/images/banner.png';

export const News = () => {
  return (
    <div className={styles.news}>
      <h3 className={styles.news__title}>NEWS</h3>
      <img className={styles.news__banner} src={banner} alt='banner'/>
    </div>
  )
}