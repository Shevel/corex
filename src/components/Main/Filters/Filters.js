import React from 'react';
import styles from './Filters.module.scss';

export const Filters = () => {
  return (
    <div className={styles.filters}>
      <h3 className={styles.filters__title}>SORT BY</h3>
      <ul className={styles.filters__list}>
        <li className={styles.filters__item}>
          By Manufacturer
          <span className={styles.dot}></span>
          <span className={styles.dot}></span>
          <span className={styles.dot}></span>
          <span className={styles.dot}></span>
        </li>
        <li className={styles.filters__item}>
          Minimum price
          <span className={styles.dot}></span>
          <span className={styles.dot}></span>
          <span className={styles.dot}></span>
          <span className={styles.dot}></span>
        </li>
        <li className={styles.filters__item}>
          Maximum price
          <span className={styles.dot}></span>
          <span className={styles.dot}></span>
          <span className={styles.dot}></span>
          <span className={styles.dot}></span>
        </li>
      </ul>      
    </div>
  )
}