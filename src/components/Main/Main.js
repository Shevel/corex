import React from 'react';
import styles from './Main.module.scss';
import { Filters } from './Filters/Filters';
import { OnSale } from './OnSale/OnSale';
import { News } from './News/News';

export const Main = () => {
  return (
    <main className={styles.main}>
      <Filters />
      <OnSale />
      <News />
    </main>
  )
}