import React from 'react';
import styles from './Footer.module.scss';
import Facebook from '../../assets/Icons/Fb.svg';
import Reddit from '../../assets/Icons/Rdd.svg';
import Twitter from '../../assets/Icons/Tw.svg';
import YouTube from '../../assets/Icons/You.svg';

export const Footer = () => {
  return (
    <footer className={styles.footer}>
      <div className={styles.socials}>
        <div className={styles.socials__item}>
          <img className={styles.socials__icon} src={Facebook} alt='facebook'/>
        </div>
        <div className={styles.socials__item}>
          <img className={styles.socials__icon} src={Twitter} alt='twitter'/>
        </div>
        <div className={styles.socials__item}>
          <img className={styles.socials__icon} src={YouTube} alt='youtube'/>
        </div>
        <div className={styles.socials__item}>
          <img className={styles.socials__icon} src={Reddit} alt='reddit'/>
        </div>
      </div>
    </footer>
  )
}